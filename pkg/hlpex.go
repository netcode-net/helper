package helper

import (
	"encoding/json"
	"errors"
	"log"
)

const MW_ERROR = "Middleware Error"
const EMPTY_STR = ""
const DEF_ERR_RECOM = "Contact maintainer of middleware."

type Er struct {
	UserMsg   string `json:"UserMsg"`
	AdmMsg    string `json:"AdmMsg"`
	UserRecom string `json:"UserRecom"`
	AdmRecom  string `json:"Admecom"`
	Cause     string `json:"Cause"`
}

func (this *Er) ToJSON() error {
	serialized, err := json.Marshal(this)
	if err != nil {
		return errors.New("Fatal serialization failure in helper.Er.toJSON().")
	}
	return errors.New(string(serialized))
}

func ToEr(e error) (*Er, error) {
	er := &Er{}
	asBytes := []byte(e.Error())
	err := json.Unmarshal(asBytes, er)
	if err != nil {
		return nil, errors.New("Fatal deserialization failure in helper.Er.ToEr().")
	}
	return er, nil
}

func EnrichAdminMsg(e error, enrich string) error {
	er, err := ToEr(e)
	if err != nil {
		return errors.New(enrich + " " + e.Error())
	}
	er.AdmMsg = enrich + " " + er.AdmMsg
	return er.ToJSON()
}

func EnrichCause(e error, enrich string) error {
	er, err := ToEr(e)
	if err != nil {
		return errors.New(enrich + " " + e.Error())
	}
	er.Cause = enrich + " " + er.Cause
	return er.ToJSON()
}

func LogFatIf(err error) {
	if err != nil {
		log.Fatal(err.Error())
	}
}

func LogIf(err error) {
	if err != nil {
		log.Println(err.Error())
	}
}
