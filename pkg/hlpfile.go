package helper

import (
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

func WithoutExt(s string) string {
	extension := filepath.Ext(s)
	withoutSuffix := strings.TrimSuffix(s, extension)
	return withoutSuffix
}

func Exists(fullPath string) bool {
	result := false
	_, err := os.Stat(fullPath)
	result = err == nil
	return result
}

func FindFilenamesMatchingIn(dirpathFull string, rgxpattern string) []string {
	var result []string
	direntries, err := os.ReadDir(dirpathFull)
	LogFatIf(err)
	for _, direntry := range direntries {
		ismatch, err := regexp.MatchString(rgxpattern, direntry.Name())
		LogFatIf(err)
		if !ismatch {
			continue
		}
		result = append(result, direntry.Name())
	}
	return result
}

// Read-, Write- and Execute-permissions
func Write777(fullpath string, data []byte) error {
	return os.WriteFile(fullpath, data, 0777)
}

// Read-, Write-permissions
func Write666(fullpath string, data []byte) error {
	return os.WriteFile(fullpath, data, 0666)
}

func MakeSureDirsExists777(path ...string) error {
	currentpathAsStr := strings.Join(path, string(os.PathSeparator))
	err := os.MkdirAll(currentpathAsStr, 0777)
	return err
}
