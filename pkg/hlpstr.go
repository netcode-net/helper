package helper

import (
	"crypto/md5"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func LineSep() string {
	return fmt.Sprintln()
}

func XTimes(toRepeat string, x int) string {
	result := ""
	for i := 0; i < x; i++ {
		result += toRepeat
	}
	return result
}

func IsEmpty(s string) bool {
	return strings.TrimSpace(s) == ""
}

func SIP(str string, vars ...string) string {
	for i := 0; i < len(vars); i++ {
		placeholderNumAsStr := strconv.Itoa(i + 1)
		str = strings.ReplaceAll(str, "%"+placeholderNumAsStr, vars[i])
	}
	return str
}

func RemoveEmpty(arr *[]string) *[]string {
	result := make([]string, 0)
	for _, val := range *arr {
		if IsEmpty(val) {
			continue
		}
		result = append(result, val)
	}
	return &result
}

func EliminateDuplicates(values *[]string) *[]string {
	result := make([]string, 0)
	set := make(map[string]bool)
	for _, val := range *values {
		if _, ok := set[val]; !ok {
			result = append(result, val)
			set[val] = true
		}
	}
	return &result
}

func Filter[T interface{}](arr []T, isRemove func(element T) bool) []T {
	filtered := make([]T, 0)
	for _, val := range arr {
		if !isRemove(val) {
			filtered = append(filtered, val)
		}
	}
	return filtered
}

func SplitTrimSpaces(s string, sep string) []string {
	filenames := strings.Split(s, sep)
	for i, filename := range filenames {
		filenames[i] = strings.TrimSpace(filename)
	}
	return filenames
}

func Split2(txt string, sep string, fieldelimiter string, fieldelimiterEsc string, removeFSep bool) []string {
	var result []string

	space := " "
	spacelen := len(space)
	seplen := len(sep)
	fdellen := len(fieldelimiter)
	fdelesclen := len(fieldelimiterEsc)
	var fielddelimitercount int
	lastSplitPos := 0

	posLastFoundSep := -1
	posLastFoundFieldDelimiter := -1
	posLastFoundSpace := -1
	posLastFoundNonSpace := -1

	for i := range txt {

		if isFoundAtPos(fieldelimiter, cut(txt, i, fdellen)) {
			isEsc := false
			if fieldelimiter == fieldelimiterEsc {
				txtchunk := cut(txt, i+fdellen, fdellen)
				isEsc = isEscape(txtchunk, fieldelimiter)
			}
			if isEsc {
				txtchunk := cut(txt, i+fdellen, fdelesclen)
				isEsc = isEscape(txtchunk, fieldelimiter)
			}
			// e.g. escape '"' with '"' = '""'.
			textchunk := cut(txt, i-fdelesclen, fdelesclen)
			if !isEscaped(textchunk, fieldelimiterEsc) && !isEsc {
				posLastFoundFieldDelimiter = i
			}
		}

		if !IsEmpty(fieldelimiter) &&
			isEven(fielddelimitercount) &&
			posLastFoundFieldDelimiter == i &&
			isFDelAfterSepIgnoreSpace(posLastFoundSep, posLastFoundSpace, posLastFoundNonSpace) {
			fielddelimitercount++
		}

		if !IsEmpty(fieldelimiter) &&
			!isEven(fielddelimitercount) &&
			posLastFoundFieldDelimiter == i &&
			isFDelBeforeSepLookahead(txt[i+1:], sep, space) {
			fielddelimitercount++
		}

		if isFoundAtPos(sep, cut(txt, i, seplen)) {
			posLastFoundSep = i
		}

		if isFoundAtPos(space, cut(txt, i, spacelen)) {
			posLastFoundSpace = i
		} else {
			posLastFoundNonSpace = i
		}

		if isEven(fielddelimitercount) && isFoundAtPos(sep, txt[i:i+seplen]) {
			cell := extract(txt, lastSplitPos, i, fieldelimiter, fdellen, removeFSep)
			cell = strings.ReplaceAll(cell, fieldelimiterEsc+fieldelimiter, fieldelimiter)
			result = append(result, cell)
			lastSplitPos = i + seplen
		}

		if isEndReached(i, txt) {
			cell := extract(txt, lastSplitPos, i+1, fieldelimiter, fdellen, removeFSep)
			result = append(result, cell)
		}
	}
	return result
}

type Tokentype int

const (
	SEPARATOR     Tokentype = 0
	FIELDELIMITER Tokentype = 1
)

type Tokenrole int

const (
	TEXT_ROLE                              = 0
	SEPARATOR_ROLE                         = 1
	OPENING_FIELD_DELIMITER_ROLE Tokenrole = 2
	CLOSING_FIELD_DELIMITER_ROLE Tokenrole = 3
)

type NamedPositions []NamedPos
type NamedPos struct {
	tokentype Tokentype
	tokenrole Tokenrole
	pos       int
}

func Split(txt string, sep string, fieldelimiter string, fieldelimiterEsc string, isRemoveFSep bool) []string {
	var result []string

	if fieldelimiter == sep {
		log.Fatalln(SIP("Fielddelimiter (%1) and seperator (%2) must not be the same"), fieldelimiter, sep)
	}

	namedPositions := make(NamedPositions, 0)
	namedPositions = append(namedPositions, NamedPos{tokentype: SEPARATOR, tokenrole: SEPARATOR_ROLE, pos: -1})
	getPositionsOfSepAndFieldDelimiter(txt, sep, fieldelimiter, &namedPositions)
	namedPositions = append(namedPositions, NamedPos{tokentype: SEPARATOR, tokenrole: SEPARATOR_ROLE, pos: len(txt)})

	for i := range namedPositions {
		if namedPositions[i].tokentype == SEPARATOR {
			effsepidx := namedPositions.GetIndexOfEffectiveBefore(i, SEPARATOR)
			isOpeningFieldDelimiterBefore := namedPositions.IsTokenWithRoleBetween(effsepidx, i, OPENING_FIELD_DELIMITER_ROLE)
			if !isOpeningFieldDelimiterBefore {
				namedPositions[i].tokenrole = SEPARATOR_ROLE
				continue
			}
			isClosingFieldDelimiterBefore := namedPositions.IsTokenWithRoleBetween(effsepidx, i, CLOSING_FIELD_DELIMITER_ROLE)
			if isOpeningFieldDelimiterBefore && isClosingFieldDelimiterBefore {
				namedPositions[i].tokenrole = SEPARATOR_ROLE
			}
		}
		if namedPositions[i].tokentype == FIELDELIMITER {
			//effseppos := namedPositions.GetPosOfEffectiveBefore(i, SEPARATOR)

			isOpeningFieldSep := namedPositions.isOpeningFieldDelimiter(i, txt)
			if isOpeningFieldSep {
				namedPositions[i].tokenrole = OPENING_FIELD_DELIMITER_ROLE
				continue
			}

			isClosingFieldDel := namedPositions.isPotentialClosingFieldDelimiter(i, txt)
			isEscaped := namedPositions.IsEscaped(i, txt, fieldelimiterEsc)
			existsOpeningFieldDel := namedPositions.existsOpeningFieldDelimiterFor(i, txt)

			if isClosingFieldDel && existsOpeningFieldDel && !isEscaped {
				namedPositions[i].tokenrole = CLOSING_FIELD_DELIMITER_ROLE
			}
		}
	}

	result = namedPositions.extractFields(txt)

	if isRemoveFSep {
		result = removeFieldDelimiter(result, fieldelimiter)
	}

	if len(strings.TrimSpace(fieldelimiter)) > 0 {
		result = RemoveEscapes(result, fieldelimiter, fieldelimiterEsc)
	}

	return result
}

func (n NamedPositions) IsEscaped(i int, txt string, fdelesc string) bool {
	result := false

	isEmptyString := n.IsEmptyStringSpecialCase(i, txt)
	if isEmptyString {
		return result
	}

	posOfFDel := n[i].pos
	posOfEsc := posOfFDel - len(fdelesc)
	if cut(txt, posOfEsc, len(fdelesc)) == fdelesc {
		result = true
	}
	return result
}

func (n NamedPositions) IsEmptyStringSpecialCase(i int, txt string) bool {
	// Checks for ,"",
	result := false
	if i-2 < 0 {
		return result
	}
	if i+1 >= len(n) {
		return result
	}

	idxSep1 := i - 2
	idxFDel1 := i - 1
	idxFDel2 := i
	idxSep2 := i + 1

	isSep1Found := n[idxSep1].tokenrole == SEPARATOR_ROLE
	isFDel1Found := n[idxFDel1].tokenrole == OPENING_FIELD_DELIMITER_ROLE
	isFDel2Found := n[idxFDel2].tokentype == FIELDELIMITER
	isSep2Found := n[idxSep2].tokentype == SEPARATOR

	if !isSep1Found || !isFDel1Found || !isFDel2Found || !isSep2Found {
		return result
	}

	txtBetween1 := cut(txt, n[idxSep1].pos+1, n[idxFDel1].pos-(n[idxSep1].pos+1))
	txtBetween2 := cut(txt, n[idxFDel1].pos+1, n[idxFDel2].pos-(n[idxFDel1].pos+1))
	txtBetween3 := cut(txt, n[idxFDel2].pos+1, n[idxSep2].pos-(n[idxFDel2].pos+1))

	txtConcat := txtBetween1 + txtBetween2 + txtBetween3
	txtConcat = strings.TrimSpace(txtConcat)
	if txtConcat == "" {
		result = true
	}

	return result
}

func (n NamedPositions) isOpeningFieldDelimiter(i int, txt string) bool {
	result := true
	if n[i].tokentype != FIELDELIMITER {
		result = false
		return result
	}

	idxBefore := i - 1
	if idxBefore < 0 {
		result = false
		return result
	}

	if n[idxBefore].tokenrole != SEPARATOR_ROLE {
		result = false
		return result
	}

	seppos := n[idxBefore].pos
	fdelpos := n[i].pos
	textBetween := cut(txt, seppos+1, fdelpos-(seppos+1))
	trimmed := strings.Trim(textBetween, "\t ")
	if len(trimmed) != 0 {
		result = false
		return result
	}
	return result
}

func (n NamedPositions) isPotentialClosingFieldDelimiter(i int, txt string) bool {
	result := true
	if n[i].tokentype != FIELDELIMITER {
		result = false
		return result
	}

	idxAfter := i + 1
	if idxAfter >= len(n) {
		result = false
		return result
	}

	if n[idxAfter].tokentype != SEPARATOR {
		result = false
		return result
	}

	seppos := n[idxAfter].pos
	fdelpos := n[i].pos
	textBetween := cut(txt, fdelpos+1, seppos-(fdelpos+1))
	trimmed := strings.Trim(textBetween, "\t ")
	if len(trimmed) != 0 {
		result = false
		return result
	}
	return result
}

func (n NamedPositions) existsOpeningFieldDelimiterFor(i int, txt string) bool {
	result := true

	isOpeningFieldDelimiterFoundFirst := false

	for j := i - 1; j >= 0; j-- {
		if n[i].tokenrole == SEPARATOR_ROLE {
			break
		}
		if n[i].tokenrole == OPENING_FIELD_DELIMITER_ROLE {
			isOpeningFieldDelimiterFoundFirst = true
			break
		}
	}

	if isOpeningFieldDelimiterFoundFirst {
		result = true
	}

	return result
}

func getPositionsOfSepAndFieldDelimiter(txt string, sep string, fieldelimiter string, namedPositions *NamedPositions) {
	for i := 0; i < len(txt); i++ {
		if txt[i:i+len(sep)] == sep {
			*namedPositions = append(*namedPositions, NamedPos{tokentype: SEPARATOR, tokenrole: TEXT_ROLE, pos: i})
		} else if strings.TrimSpace(fieldelimiter) != "" && txt[i:i+len(fieldelimiter)] == fieldelimiter {
			*namedPositions = append(*namedPositions, NamedPos{tokentype: FIELDELIMITER, tokenrole: TEXT_ROLE, pos: i})
		}
	}
}

func (n NamedPositions) GetIndexOfEffectiveBefore(idx int, tokentype Tokentype) int {
	result := -1
	for i := idx - 1; i > 0; i-- {
		if n[i].tokentype == tokentype && n[i].IsEffective() {
			result = i
			break
		}
	}
	return result
}
func (n NamedPositions) GetPosOfEffectiveBefore(idx int, tokentype Tokentype) int {
	result := -1
	for i := idx - 1; i > 0; i-- {
		if n[i].tokentype == tokentype && n[i].IsEffective() {
			result = n[i].pos
		}
	}
	return result
}

func (n NamedPositions) IsTokenWithRoleBetween(startpos int, endpos int, role Tokenrole) bool {
	result := false
	for i := endpos - 1; i > startpos; i-- {
		if n[i].tokenrole == role {
			result = true
			break
		}
	}
	return result
}

func (n NamedPositions) extractFields(txt string) []string {
	positions := n.positionsOf(SEPARATOR_ROLE)
	fields := make([]string, 0)
	for i := 0; i < len(positions)-1; i++ {
		start := positions[i] + 1
		end := positions[i+1]
		field := txt[start:end]
		fields = append(fields, field)
	}
	return fields
}

func (n NamedPositions) positionsOf(tokenrole Tokenrole) []int {
	positions := make([]int, 0)
	for _, namedpos := range n {
		if namedpos.tokenrole == tokenrole {
			positions = append(positions, namedpos.pos)
		}
	}
	return positions
}

func (n NamedPos) IsEffective() bool {
	result := false
	if n.tokenrole == SEPARATOR_ROLE || n.tokenrole == OPENING_FIELD_DELIMITER_ROLE || n.tokenrole == CLOSING_FIELD_DELIMITER_ROLE {
		result = true
	}
	return result
}

func AllIndices(str string, substr string) []int {
	seppositions := make([]int, 0)
	for i := 0; i < len(str); i++ {
		foundPos := strings.Index(str[i:], substr)
		if foundPos != -1 {
			seppositions = append(seppositions, foundPos)
			i += foundPos
		}
	}
	return seppositions
}

func cut(txt string, start int, cutlen int) string {
	cutpos := start + cutlen
	if start < 0 {
		start = 0
	}
	if cutpos > len(txt) {
		cutpos = len(txt)
	}
	return txt[start:cutpos]
}

func removeFieldDelimiter(fields []string, fdel string) []string {
	result := make([]string, 0)
	for _, field := range fields {
		trimmedField := strings.TrimSpace(field)
		isEnclosedWithFDel := IsEnclosedWith(trimmedField, fdel)
		if isEnclosedWithFDel {
			fdelLen := len(fdel)
			field = trimmedField[fdelLen : len(trimmedField)-fdelLen]
		}
		result = append(result, field)
	}
	return result
}

func RemoveEscapes(fields []string, fdel string, fdelEsc string) []string {
	result := make([]string, 0)
	for _, field := range fields {
		// special case, empty string
		if field != "\"\"" {
			field = strings.ReplaceAll(field, fdelEsc+fdel, fdel)
		}
		result = append(result, field)
	}
	return result
}

func IsEnclosedWith(str string, with string) bool {
	result := false
	fdelLen := len(with)
	if len(str) < fdelLen*2 {
		return false
	}

	firstidx := 0
	lastidx := len(str) - fdelLen
	result = cut(str, firstidx, fdelLen) == with && cut(str, lastidx, fdelLen) == with

	return result
}

func isFoundAtPos(tofind string, s string) bool {
	return tofind == s
}

func isEscaped(txt string, escape string) bool {
	return txt == escape
}
func isEscape(txt string, escape string) bool {
	return txt == escape
}

func isFDelAfterSepIgnoreSpace(posLastFoundSep int, posLastFoundSpace int, posLastFoundNonSpace int) bool {
	return posLastFoundNonSpace == posLastFoundSep
}

func isFDelBeforeSepLookahead(txt string, sep string, space string) bool {
	result := true
	seplen := len(sep)
	spacelen := len(space)

	for i := range txt {
		if isFoundAtPos(sep, cut(txt, i, i+seplen)) {
			break
		}
		if !isFoundAtPos(space, cut(txt, i, i+spacelen)) {
			result = false
			break
		}
	}
	return result
}

func extract(s string, lastSplitPos int, i int, fieldlimiter string, flen int, removeFSep bool) string {
	cell := s[lastSplitPos:i]
	clen := len(cell)

	cell = strings.TrimSpace(cell)
	if !removeFSep {
		return cell
	}
	cend := Min(clen, flen)
	if isFoundAtPos(fieldlimiter, cell[:cend]) {
		cell = cell[flen:]
	}
	cstart := Max(0, len(cell)-flen)
	if isFoundAtPos(fieldlimiter, cell[cstart:]) {
		cell = cell[:len(cell)-flen]
	}
	return cell
}

func isEven(i int) bool {
	return i%2 == 0
}

func isEndReached(pos int, s string) bool {
	return pos == len(s)-1
}

func HashMD5(toHash string) string {
	s := []byte(toHash)
	hmd5 := md5.Sum([]byte(s))
	result := fmt.Sprintf("%x", hmd5)
	return result
}

func CorrectWinUnixPathSeps(path string) string {
	windowssep := "\\"
	unixsep := "/"

	ossep := fmt.Sprintf("%c", os.PathSeparator)
	if ossep == windowssep {
		path = strings.ReplaceAll(path, unixsep, windowssep)
	} else if ossep == unixsep {
		path = strings.ReplaceAll(path, windowssep, unixsep)
	}
	return path
}
