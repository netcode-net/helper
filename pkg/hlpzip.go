package helper

import (
	"archive/zip"
	"bytes"
	"compress/gzip"
	"io"
)

var NIL []byte = nil
var NOERROR error = nil

func DecompressGZIP(file []byte) ([]byte, error) {
	bReader := bytes.NewReader(file)

	gzipreader, err := gzip.NewReader(bReader)

	if err != nil {
		return NIL, err
	}

	result, err := io.ReadAll(gzipreader)
	if err != nil {
		return NIL, err
	}
	return result, NOERROR
}

func DecompressZIP(file []byte) (map[string][]byte, error) {
	result := make(map[string][]byte, 0)

	bReader := bytes.NewReader(file)

	zipreader, err := zip.NewReader(bReader, bReader.Size())
	if err != nil {
		return nil, err
	}

	for _, fileInZip := range zipreader.File {
		fileInZipReader, err := fileInZip.Open()
		if err != nil {
			return nil, err
		}
		defer fileInZipReader.Close()
		fileAsBytes, err := io.ReadAll(fileInZipReader)
		if err != nil {
			return nil, err
		}
		result[fileInZip.Name] = fileAsBytes

	}
	return result, NOERROR
}
