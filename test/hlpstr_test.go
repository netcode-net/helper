package helper

import (
	"reflect"
	"testing"

	helper "gitlab.com/reputatio/development/standards/middleware/helper/pkg"
)

func Test_split(t *testing.T) {

	type args struct {
		s                 string
		sep               string
		fieldlimiter      string
		fielddelimiterEsc string
		removeFSep        bool
	}

	tests := []struct {
		name string
		args args
		want []string
	}{
		/*{
		name: "splitxxx if fielddelimiter is empty doesn't work.",
		args: args{
			s:                 "'cel,l1','cell2',cell3",
			sep:               ",",
			fieldlimiter:      "'",
			fielddelimiterEsc: "'",
			removeFSep:        false},
		want: []string{"'cel,l1'", "'cell2'", "cell3"},*/
		{
			name: "split0 BC format.",
			args: args{
				s:                 "\"ITScopeId\",\"IcecatId\",\"EAN\"",
				sep:               ",",
				fieldlimiter:      "\"",
				fielddelimiterEsc: "\"",
				removeFSep:        true},
			want: []string{"ITScopeId", "IcecatId", "EAN"},
		},
		{
			name: "split1",
			args: args{
				s:                 "'cel,l1','cell2',cell3",
				sep:               ",",
				fieldlimiter:      "'",
				fielddelimiterEsc: "'",
				removeFSep:        false},
			want: []string{"'cel,l1'", "'cell2'", "cell3"},
		},
		{
			name: "split2",
			args: args{
				s:                 "'cell1', 'cel,l2','cell3', cell4",
				sep:               ",",
				fieldlimiter:      "'",
				fielddelimiterEsc: "'",
				removeFSep:        true},
			want: []string{"cell1", "cel,l2", "cell3", " cell4"},
		},
		{
			name: "split3",
			args: args{
				s:                 "\"cell0\",cel\"l1,\"cell2\",  \"cell3\"",
				sep:               ",",
				fieldlimiter:      "\"",
				fielddelimiterEsc: "\"",
				removeFSep:        true},
			want: []string{"cell0", "cel\"l1", "cell2", "cell3"},
		},
		{
			name: "split4 icecat proddata-format",
			args: args{
				s:                 "\"Txt1\"\",\"xyz\", Txt2,Txt3",
				sep:               ",",
				fieldlimiter:      "\"",
				fielddelimiterEsc: "\"",
				removeFSep:        true},
			want: []string{"Txt1\",\"xyz", " Txt2", "Txt3"},
		}, {
			name: "split icecat genprod format",
			args: args{
				s:                 "\"\", \"\" ,\"Samsung Galaxy S21 ... Weiß\",\"6.4\"\", ...Mehrfachkamera, ... \"",
				sep:               ",",
				fieldlimiter:      "\"",
				fielddelimiterEsc: "\"",
				removeFSep:        true,
			},
			want: []string{"", "", "Samsung Galaxy S21 ... Weiß", "6.4\", ...Mehrfachkamera, ... "},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := helper.Split(tt.args.s, tt.args.sep, tt.args.fieldlimiter, tt.args.fielddelimiterEsc, tt.args.removeFSep); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("split() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSplit(t *testing.T) {
	type args struct {
		s                 string
		sep               string
		fieldlimiter      string
		fielddelimiterEsc string
		removeFSep        bool
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := helper.Split(tt.args.s, tt.args.sep, tt.args.fieldlimiter, tt.args.fielddelimiterEsc, tt.args.removeFSep); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Split() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSplit2(t *testing.T) {
	type args struct {
		txt              string
		sep              string
		fieldelimiter    string
		fieldelimiterEsc string
		removeFSep       bool
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Hard CSV Test",
			args: args{txt: "\"Hal,lo\",Hal\"lo,Hallo", sep: ",", fieldelimiter: "\"", fieldelimiterEsc: "\\\""},
			want: []string{"\"Hal,lo\"", "Hal\"lo", "Hallo"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := helper.Split(tt.args.txt, tt.args.sep, tt.args.fieldelimiter, tt.args.fieldelimiterEsc, tt.args.removeFSep); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Split2() = %v, want %v", got, tt.want)
			}
		})
	}
}
